package com.teadust.vocabulary.utils;

import java.io.File;

public class FileUtils {

    /**
     * @param file selected file.
     * @return Only name without path and extension.
     */
    public static String getName(File file) {
        if (file.isDirectory()) {
            return file.getName();
        }
        return file.getName().replaceFirst("[.][^.]+$", "");
    }

    /**
     * @param file selected file.
     * Pay attention that extension returns in lower case mode.
     */
    public static String getExtension(File file) {
        String fileName = file.getName().toLowerCase();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return null;
    }
}
