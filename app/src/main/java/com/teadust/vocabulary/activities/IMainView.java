package com.teadust.vocabulary.activities;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.widget.TextView;

public interface IMainView {

    Context getContext();
    TextView getText();

    void initUI();

    void onNavigationMenuOpenFileClick();
    void onNavigationMenuSettingsClick();
    NavigationView.OnNavigationItemSelectedListener navigationMenuListener();
}
