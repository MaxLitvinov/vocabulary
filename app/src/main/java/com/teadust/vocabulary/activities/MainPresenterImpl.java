package com.teadust.vocabulary.activities;

import android.widget.Toast;

import com.teadust.vocabulary.dialogs.open_file_dialog.FileChooser;
import com.teadust.vocabulary.dialogs.open_file_dialog.FileSelectedListener;
import com.teadust.vocabulary.utils.FileUtils;

import java.io.File;

public class MainPresenterImpl implements IMainPresenter {

    private static final String TAG = MainPresenterImpl.class.getSimpleName();

    private IMainView iMainView;

    public MainPresenterImpl(IMainView view) {
        iMainView = view;
    }

    @Override
    public void showOpenFileDialog() {
        new FileChooser(iMainView.getContext()).setFileListener(new FileSelectedListener() {
            @Override
            public void fileSelected(File file) {
                showText(file);
            }
        }).showDialog();
    }

    @Override
    public void showSettings() {
        // TODO: settings handling.
        Toast.makeText(iMainView.getContext(), "Settings menu in development mode", Toast.LENGTH_SHORT).show();
    }

    private void showText(File file) {
        String fileName = FileUtils.getName(file);
        fileName = String.format("Filname: %13s", fileName);

        String fileExtension = FileUtils.getExtension(file);
        fileExtension = String.format("File extension: %2s", fileExtension);

        StringBuilder builder = new StringBuilder();
        builder.append(fileName).append("\n").append(fileExtension);

        iMainView.getText().setText(builder.toString());
    }
}
