package com.teadust.vocabulary.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.teadust.vocabulary.R;

public class MainActivity extends AppCompatActivity implements IMainView {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private NavigationView mNavigationView;

    private TextView mText;

    private IMainPresenter iMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initPresenter();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public TextView getText() {
        return mText;
    }

    @Override
    public void initUI() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(navigationMenuListener());

        mText = (TextView) findViewById(R.id.tv_text);
    }

    private void initPresenter() {
        iMainPresenter = new MainPresenterImpl(this);
    }

    @Override
    public void onNavigationMenuOpenFileClick() {
        iMainPresenter.showOpenFileDialog();
    }

    @Override
    public void onNavigationMenuSettingsClick() {
        iMainPresenter.showSettings();
    }

    @Override
    public NavigationView.OnNavigationItemSelectedListener navigationMenuListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.navigation_menu_open_file) {
                    onNavigationMenuOpenFileClick();
                } else if (id == R.id.navigation_view_menu_settings) {
                    onNavigationMenuSettingsClick();
                }

                mDrawerLayout.closeDrawer(GravityCompat.START);

                return true;
            }
        };
    }
}