package com.teadust.vocabulary.activities;

public interface IMainPresenter {

    void showOpenFileDialog();
    void showSettings();
}
