package com.teadust.vocabulary.dialogs.open_file_dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class FileAdapter extends ArrayAdapter<String> {

    public FileAdapter(Context context, String[] fileList) {
        super(context, android.R.layout.simple_list_item_1, fileList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        ((TextView) convertView).setSingleLine(true);
        return convertView;
    }
}
