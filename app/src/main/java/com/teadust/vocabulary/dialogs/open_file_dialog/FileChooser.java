package com.teadust.vocabulary.dialogs.open_file_dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class FileChooser {

    private static final String PARENT_DIR = "..";

    private final Context mContext;
    private ListView mListView;
    private Dialog mDialog;
    private File mCurrentPath;

    private FileSelectedListener mFileListener;

    public FileChooser(Context context) {
        mContext = context;
        mDialog = new Dialog(mContext);
        mListView = new ListView(mContext);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String fileChosen = (String) mListView.getItemAtPosition(position);
                File chosenFile = getChosenFile(fileChosen);
                if (chosenFile.isDirectory()) {
                    refresh(chosenFile);
                } else {
                    if (mFileListener != null) {
                        mFileListener.fileSelected(chosenFile);
                    }
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(mListView);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        refresh(Environment.getExternalStorageDirectory());
    }

    public FileChooser setFileListener(FileSelectedListener listener) {
        mFileListener = listener;
        return this;
    }

    private File getChosenFile(String fileChosen) {
        if (fileChosen.equals(PARENT_DIR)) {
            return mCurrentPath.getParentFile();
        }
        return new File(mCurrentPath, fileChosen);
    }

    private void refresh(File path) {
        mCurrentPath = path;
        if (path.exists()) {
            File[] dirs = getDirs(path);
            File[] files = getFiles(path);

            String[] fileList = initList(path, dirs, files);
            Arrays.sort(dirs);
            Arrays.sort(files);
            fileList = getList(fileList, dirs);
            fileList = getList(fileList, files);

            mDialog.setTitle(mCurrentPath.getPath());
            mListView.setAdapter(new FileAdapter(mContext, fileList));
        }
    }

    private File[] getDirs(File path) {
        return path.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return (pathname.isDirectory() && pathname.canRead());
            }
        });
    }

    private File[] getFiles(File path) {
        return path.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (!pathname.isDirectory() && pathname.canRead()) {
                    // TODO: filter file extension here.
                    return true;
                }
                return false;
            }
        });
    }

    private String[] initList(File path, File[] dirs, File[] files) {
        String[] fileList;
        if (path.getParentFile() == null) {
            fileList = new String[dirs.length + files.length];
        } else {
            fileList = new String[dirs.length + files.length + 1];
            fileList[0] = PARENT_DIR;
        }
        return fileList;
    }

    private String[] getList(String[] fileList, File[] files) {
        int position = getLastAddedPosition(fileList);
        for (File file : files) {
            fileList[position++] = file.getName();
        }
        return fileList;
    }

    private int getLastAddedPosition(String[] fileList) {
        int lastAddedPosition = 0;
        for (String file : fileList) {
            if (file != null) {
                lastAddedPosition++;
            }
        }
        return lastAddedPosition;
    }

    public void showDialog() {
        mDialog.show();
    }
}
