package com.teadust.vocabulary.dialogs.open_file_dialog;

import java.io.File;

/**
 * File selection event handling.
 */
 public interface FileSelectedListener {

    /**
     * @param file selected file.
     */
    void fileSelected(File file);
}
